#!/bin/sh -e

<<LICENSE
MIT License

Copyright (c) 2016 Chad Braun-Duin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
LICENSE

# uncomment the following two lines if you have both BSD sed and GNU sed installed
# this script is only tested to work with GNU sed which may have the command gsed
# shopt -s expand_aliases
# alias sed=gsed

# move the original text to a temp file that can be progressively modified
temp_file="/tmp/markdown.$$"
cat "$@" > "$temp_file"

# All of this below business is for reference-style links and images
# We need to loop across newlines and not spaces
IFS='
'
refs=$(sed -nr "/^\[.+\]: +/p" "$@")
for ref in $refs
do
    ref_id=$(echo -n "$ref" | sed -nr "s/^\[(.+)\]: .*/\1/p" | tr -d '\n')
    ref_url=$(echo -n "$ref" | sed -nr "s/^\[.+\]: (.+)/\1/p" | cut -d' ' -f1 | tr -d '\n')
    ref_title=$(echo -n "$ref" | sed -nr "s/^\[.+\]: (.+) \"(.+)\"/\2/p" | sed 's@|@!@g' | tr -d '\n')

    # reference-style image using the label
    sed -ri "s|!\[([^]]+)\]\[($ref_id)\]|<img src=\"$ref_url\" title=\"$ref_title\" alt=\"\1\" />|gI" "$temp_file"
    # reference-style link using the label
    sed -ri "s|\[([^]]+)\]\[($ref_id)\]|<a href=\"$ref_url\" title=\"$ref_title\">\1</a>|gI" "$temp_file"

    # implicit reference-style
    sed -ri "s|!\[($ref_id)\]\[\]|<img src=\"$ref_url\" title=\"$ref_title\" alt=\"\1\" />|gI" "$temp_file"
    # implicit reference-style
    sed -ri "s|\[($ref_id)\]\[\]|<a href=\"$ref_url\" title=\"$ref_title\">\1</a>|gI" "$temp_file"
done

# delete the reference lines
sed -ri "/^\[.+\]: +/d" "$temp_file"

# blockquotes
# use grep to find all the nested blockquotes
while grep '^> ' "$temp_file" >/dev/null
do
    sed -nri '
/^$/b blockquote

H
$ b blockquote
b

:blockquote
x
s/(\n+)(> .*)/\1<blockquote>\n\2\n<\/blockquote>/
p
' "$temp_file"

    sed -i '1 d' "$temp_file" # cleanup superfluous first line

    # cleanup blank lines and remove subsequent blockquote characters
    sed -ri '
/^> /s/^> (.*)/\1/
' "$temp_file"
done

# Setext-style headers
sed -nri '
/^$/ b print

H
$ b print
b

:print
x
/=+$/{
s/\n(.*)\n=+$/\n<h1>\1<\/h1>/
p
b
}
/\-+$/{
s/\n(.*)\n\-+$/\n<h2>\1<\/h2>/
p
b
}
p
' "$temp_file"

sed -i '1 d' "$temp_file" # cleanup superfluous first line

# atx-style headers and other block styles
sed -ri '
/^#+ /s/ #+$//
/^# /s/# ([A-Za-z0-9 ]*)(.*)/<h1 id="\1">\1\2<\/h1>/g
/^#{2} /s/#{2} ([A-Za-z0-9 ]*)(.*)/<h2 id="\1">\1\2<\/h2>/g
/^#{3} /s/#{3} ([A-Za-z0-9 ]*)(.*)/<h3 id="\1">\1\2<\/h3>/g
/^#{4} /s/#{4} ([A-Za-z0-9 ]*)(.*)/<h4 id="\1">\1\2<\/h4>/g
/^#{5} /s/#{5} ([A-Za-z0-9 ]*)(.*)/<h5 id="\1">\1\2<\/h5>/g
/^#{6} /s/#{6} ([A-Za-z0-9 ]*)(.*)/<h6 id="\1">\1\2<\/h6>/g

/^\*\*\*+$/s/\*\*\*+/<hr \/>/
/^---+$/s/---+/<hr \/>/
/^___+$/s/___+/<hr \/>/

' "$temp_file"

# unordered lists
# use grep to find all the nested lists
while grep '^[\*\+\-] ' "$temp_file" >/dev/null
do
sed -nri '
/^$/b list


/^[\*\+\-] /s/[\*\+\-] (.*)/<\/uli>\n<uli>\n\1/

H
$ b list 
b

:list
x
/<uli>/{
s/(.*)/\n<ul>\1\n<\/uli>\n<\/ul>/
s/\n<\/uli>//
p
b
}
p
' "$temp_file"

sed -i '1 d' "$temp_file" # cleanup superfluous first line

# convert to the proper li to avoid collisions with nested lists
sed -i 's/uli>/li>/g' "$temp_file"

# prepare any nested lists
sed -ri '/^[\*\+\-] /s/(.*)/\n\1\n/' "$temp_file"
done

# ordered lists
# use grep to find all the nested lists
while grep -E '^[1-9]+\. ' "$temp_file" >/dev/null
do
sed -nri '
/^$/b list

/^[1-9]+\. /s/[1-9]+\. (.*)/<\/oli>\n<oli>\n\1/

H
$ b list
b

:list
x
/<oli>/{
s/(.*)/\n<ol>\1\n<\/oli>\n<\/ol>/
s/\n<\/oli>//
p
b
}
p
' "$temp_file"

sed -i '1 d' "$temp_file" # cleanup superfluous first line

# convert list items into proper list items to avoid collisions with nested lists
sed -i 's/oli>/li>/g' "$temp_file"

# prepare any nested lists
sed -ri '/^[1-9]+\. /s/(.*)/\n\1\n/' "$temp_file"
done

# make escaped periods literal
sed -ri '/^[1-9]+\\. /s/([1-9]+)\\. /\1\. /' "$temp_file"


# code blocks
sed -nri '
${
H
b code
}

/^\t| {4}/!b code

H
b

:code
x
/\t| {4}/{
s/(\t| {4})(.*)/<pre><code>\n\1\2\n<\/code><\/pre>/
p
b
}
p
' "$temp_file"

sed -i '1 d' "$temp_file" # cleanup superfluous first line

# convert html characters inside pre-code tags into printable representations
sed -ri '
/^<pre><code>/{
:inside
n
/^<\/code><\/pre>/!{
s/&/\&amp;/g
s/</\&lt;/g
s/>/\&gt;/g
b inside
}
}
' "$temp_file"

# remove the first tab (or 4 spaces) from the code lines
sed -ri 's/^\t| {4}(.*)/\1/' "$temp_file"

# br tags
sed -ri '
/^$/ {
N
N
/^\n{2}/s/(.*)/\n<br \/>\1/
}
' "$temp_file"

# emphasis and strong emphasis and strikethrough
sed -nri '
/^$/b emphasis

H
$ b emphasis
b

:emphasis
x
s/\*\*(.+)\*\*/<strong>\1<\/strong>/g
s/__([^_]+)__/<strong>\1<\/strong>/g
s/\*([^\*]+)\*/<em>\1<\/em>/g
s/\~\~(.+)\~\~/<strike>\1<\/strike>/g
p
' "$temp_file"

sed -i '1 d' "$temp_file" # cleanup superfluous first line

# paragraphs
sed -nri '
/^$/ b para
H
$ b para
b
:para
x
/\n<(div|table|pre|p|[ou]l|h[1-6]|[bh]r|blockquote|li)/!{
s/(\n+)(.*)/\1<p>\n\2\n<\/p>/
p
b
}
p
' "$temp_file"

sed -i '1 d' "$temp_file" # cleanup superfluous first line

# cleanup area where P tags have broken nesting
sed -nri '
/^<\/(div|table|pre|p|[ou]l|h[1-6]|[bh]r|blockquote)>/{
h
$ {
x
b done
}
n
/^<\/p>/{
G
b done
}
H
x
}
:done
p
' "$temp_file"

# inline styles and special characters
sed -ri '
s/<(http[s]?:\/\/.*)>/<a href=\"\1\">\1<\/a>/g
s/<(.*@.*\..*)>/<a href=\"mailto:\1\">\1<\/a>/g

s/([^\\])``+ *([^ ]*) *``+/\1<code>\2<\/code>/g
s/([^\\])`([^`]*)`/\1<code>\2<\/code>/g

s/!\[(.*)\]\((.*) \"(.*)\"\)/<img alt=\"\1\" src=\"\2\" title=\"\3\" \/>/g
s/!\[(.*)\]\((.*)\)/<img alt=\"\1\" src=\"\2\" \/>/g

s/\[(.*)]\((.*) "(.*)"\)/<a href=\"\2\" title=\"\3\">\1<\/a>/g
s/\[(.*)]\((.*)\)/<a href=\"\2\">\1<\/a>/g

/&.+;/!s/&/\&amp;/g
/<[\/a-zA-Z]/!s/</\&lt;/g

s/\\\*/\*/g
s/\\_/_/g
s/\\`/`/g
s/\\#/#/g
s/\\\+/\+/g
s/\\\-/\-/g
s/\\\\/\\/g
' "$temp_file"

# display and cleanup
cat "$temp_file" && rm "$temp_file"
