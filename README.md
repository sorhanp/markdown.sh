# markdown.sh

A fork of Markdown interpreter by Chad Braun-Duin known as [markdown.bash](https://github.com/chadbraunduin/markdown.bash)

## About

markdown.sh is a Markdown interpreter that uses sh, sed, grep, and cut.

## How to use

TODO